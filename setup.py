from setuptools import setup, find_packages

with open('requirements.txt') as f:
    requirements = f.read().splitlines()

setuptools.setup(
      name='MPV UDP controller',
      version='0.1',
      install_requires=requirements,
      scripts=['bin/mpv-udp'],
      description='MPV UDP controller',
      url='https://gitlab.com/makeexpose/mpv-udp',
      author='Joep Vermaat',
      author_email='joep.vermaat@naturalis.nl',
      zip_safe=False,
      classifiers=(
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: LGPL License",
        "Operating System :: OS Independent",
      ),
)
