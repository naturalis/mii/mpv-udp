# MPV UDP

The MPV UDP controller is a simple Python script that listens to either a
unicast IP address and port or to a mulicast IP address and port
combination for short UDP messsages sent by show controllers.

These messages are translated to JSON stuctures and sent to one or more
running mpv instances on the machine. This way you can do various setups with
one or many mediaplayer machines running one or more mpv instances and start,
stop, pause, unpause, mute or unmute the shows running on these machines at the
same time.

## Installation

To install and use mpvudp, you need:

* A recent version of mpv (preferably version 2.0 or newer)
* To run mpv with the JSON IPC server [as explained in the mpv
  documentation](https://mpv.io/manual/stable/#json-ipc).
* An installation of [pip3](https://pip.pypa.io/en/stable/).

You can install mpvudp by running this command:

 `pip3 install git+https://gitlab.com/naturalis/mii/mpv-udp.git#egg=mpv-udp`

## Configuration

## Yaml configuration

The configuration of this UDP tool is by yaml file. It consists of three parts:

### Networking

A typical setup listening for multicast messages on a port:

```
udp:
  ip: '224.0.1.1'
  port: 8001
  multicast: yes
```

Or unicast this would be:

```
udp:
  ip: '127.0.0.1'
  port: 8001
  multicast: no
```

### Socket(s)

mpvupd will redirect messages it receives to one or multiple mpv IPC sockets.
Configure the socket(s) the following way:

```
mpv:
  socket:
    - '/tmp/mpv.sock'
```

### Commands

mpvudp allows to be configured to listen for any number of arbitrary commands,
each of which will be translated to one or several [mpv IPC
messages](https://mpv.io/manual/stable/#protocol). For example:

```
commands:
  mpvstartshow:
    -
      - 'loadlist'
      - 'key-a.txt'
    -
      - 'set_property'
      - 'ao-mute'
      - 'no'
    -
      - 'set_property'
      - 'ao-volume'
      - '75'
```

## Starting the script

You can start the script by command:

```
mpv-udp
```

This will look for a `config.yaml` in the current directory. You can specify
a different config file using the --config argument.

```
mpv-udp --config=config.yaml
```
